jQuery(document).ready(function ($) {
    $('.blog__tab').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
    })


    var mainServices = new Swiper('.main-team-slider', {
        speed: 400,
        spaceBetween: 30,
        slidesPerView: 'auto',
        centeredSlides: false,
        loop: true,

        pagination: {
            el: '.main-team-pagination',
            type: 'fraction',
        },
        navigation: {
            nextEl: '.main-team-next',
            prevEl: '.main-team-prev',
        },
    });


    var aboutPartners = new Swiper('.about-partners-swiper', {
        speed: 400,
        spaceBetween: 20,
        slidesPerView: 'auto',
        centeredSlides: false,
        loop: true,

        pagination: {
            el: '.about-partners-pagination',
            type: 'fraction',
        },
    });

    var aboutTeam = new Swiper('.about-team-swiper', {
        speed: 400,
        spaceBetween: 20,
        slidesPerView: 'auto',
        centeredSlides: false,
        loop: true,

        pagination: {
            el: '.about-team-pagination',
            type: 'fraction',
        },
    });

    $('.link-scroll').click(function (event) {
        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;

            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 800, function () {
                window.location.hash = hash;
            });
        }
    });

    $('.js-cookie-close').on('click', function (event) {
        event.preventDefault();
        $('.cookie').addClass('cookie-close');
    })
});
